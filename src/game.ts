import { PlayerShip } from "./logic/player-ship";
import { CollisionManager } from "./logic/collision-manager";
import { BoardVisuals } from "./visual/board-visuals";
import { GameScreen } from "./visual/game-screen";
import { Projectile } from "./logic/projectile";
import { EnemyShipManager } from "./logic/enemy-ship-manager";
import { WaveManager } from "./logic/wave-manager";
import { MenuVisuals } from "./visual/menu-visuals";
import { BoardOverlay } from "./visual/board-overlay";

export class Game {

    public static instance: Game;

    private gameActive: boolean;
    private gameScreen: GameScreen;
    private collisionManager: CollisionManager;
    private player: PlayerShip;
    private enemies: EnemyShipManager;
    private waveManager: WaveManager;
    private boardVisuals: BoardVisuals;
    private menuVisuals: MenuVisuals;
    private boardOverlay: BoardOverlay;
    
    private arrowLeft: boolean;
    private arrowRight: boolean;
    private arrowUp: boolean;
    private arrowDown: boolean;
    private spacebar: boolean;

    public score: number;
    public level: number;

    constructor() {
        Game.instance = this;
        this.gameActive = false;
        this.gameScreen = new GameScreen();
        this.collisionManager = new CollisionManager();
        this.player = new PlayerShip();
        this.enemies = new EnemyShipManager();
        this.waveManager = new WaveManager();
        this.boardVisuals = new BoardVisuals();
        this.menuVisuals = new MenuVisuals();
        this.boardOverlay = new BoardOverlay();

        this.arrowLeft = false;
        this.arrowRight = false;
        this.arrowUp = false;
        this.arrowDown = false;
        this.spacebar = false;

        this.score = 0;
        this.level = 0;
        
        this.menuVisuals.showStartMenu();
        this.setupControls();
        this.resetGame();
        this.gameLoop();
    }

    public startGame(): void {
        this.resetGame();
        this.menuVisuals.hideMenu();
        this.enemies.spawnEnemyWave(this.waveManager.getStartingWave());
        this.enemies.isDirty = true;
        this.gameActive = true;
    }

    public nextLevel(): void {
        this.enemies.reset();
        this.enemies.spawnEnemyWave(this.waveManager.getNextWave());
    }

    public endGame(): void {
        this.gameActive = false;
        this.menuVisuals.showGameOver();
    }

    private resetGame() {
        this.enemies.reset();
        this.player.reset();
        this.score = 0;
        this.level = 1;
    }

    private gameLoop(): void {
        if (this.gameActive) {
            this.checkInput();
            if (this.enemies.enemyShips.length === 0) {
                this.nextLevel();
            }

            this.score += this.enemies.deltaScore;
            this.enemies.deltaScore = 0;
            
            this.updateBoard();
            
            this.enemies.updateShips(this.level);
            this.collisionManager.checkCollisions();
            
            
            if (this.enemies.enemiesWin || this.player.playerDied) {
                this.endGame();
            }
        } else {
            
        }
        
        this.gameScreen.render();
        requestAnimationFrame(() => this.gameLoop());
    }

    private updateBoard(): void {
        this.boardVisuals.setPlayer(this.player);
        this.boardVisuals.drawEnemies(this.enemies.enemyShips, this.enemies.isDirty);
        this.enemies.isDirty = false;

        // Debug purposes only
        // this.boardVisuals.drawColliders(this.collisionManager.activeColliders);
        
        this.player.updatePlayer();
        let projectileList: Projectile[] = [];
        projectileList.push(...this.enemies.currentProjectiles);
        projectileList.push(...this.player.currentProjectiles);
        this.boardVisuals.drawProjectiles(projectileList);

        this.boardOverlay.updateLives(this.player.currentPlayerLives);
        this.boardOverlay.updateScore(this.score);
    }

    private checkInput(): void {
        if (this.spacebar) {
            this.player.fire();
        }
        if (this.arrowRight) {
            this.player.moveHorizontal(1);
        }
        if (this.arrowLeft) {
            this.player.moveHorizontal(-1);
        }
        if (this.arrowUp) {
            this.player.moveVertical(-1);
        }
        if (this.arrowDown) {
            this.player.moveVertical(1);
        }
    }

    private setupControls(): void {
        document.addEventListener('keydown', (e) => {
            if (this.gameActive) {
                switch(e.key) {
                    case "ArrowLeft":
                        this.arrowLeft = true;
                        break;
                    case "ArrowRight":
                        this.arrowRight = true;
                        break;
                    default: break;
                }
                switch(e.key) {
                    case "ArrowUp":
                        this.arrowUp = true;
                        break;
                    case "ArrowDown":
                        this.arrowDown = true;
                        break;
                    default: break; 
                }
                if (e.key === "Space" || e.key === " ") {
                    this.spacebar = true;
                }
            }
        });
        document.addEventListener('keyup', (e) => {
            switch(e.key) {
                case "ArrowLeft":
                    this.arrowLeft = false;
                    break;
                case "ArrowRight":
                    this.arrowRight = false;
                    break;
                default: break;
            }
            switch(e.key) {
                case "ArrowUp":
                    this.arrowUp = false;
                    break;
                case "ArrowDown":
                    this.arrowDown = false;
                    break;
                default: break; 
            }
            if (e.key === "Space" || e.key === " ") {
                this.spacebar = false;
            }
        });
    }
}