import { upperCase } from "lodash";
import { Container, Graphics, Text, TextStyle } from "pixi.js";
import { Game } from "src/game";
import { GameScreen } from "./game-screen";

export class MenuVisuals {

    private menu: Container;
    private menuTitle: Text;
    private restartButton: Container;
    private buttonText: Text;

    constructor () {
        this.menu = new Container();

        this.menu.position.x = GameScreen.instance.canvasWidth / 2;
        this.menu.position.y = 300;

        this.menuTitle = new Text("Pixi Invaders");
        this.menuTitle.style = new TextStyle({fill: 0x00ff00, fontVariant: 'small-caps', fontWeight: '600', fontSize: 70});
        this.menuTitle.anchor.set(0.5, 0.5);

        this.restartButton = new Container();
        this.restartButton.position.y = 100;
        this.restartButton.interactive = true;
        this.restartButton.cursor = 'pointer';
        this.restartButton.on("mousedown", () => {
            Game.instance.startGame();
        });
        let background = new Graphics();
        this.buttonText = new Text("start");
        this.restartButton.addChild(background, this.buttonText);
        background.lineStyle(2, 0x00ff00, 1);
        background.beginFill(0x225522);
        background.drawRoundedRect(-75, 0, 150, 50, 20);
        background.endFill();
        this.buttonText.style = new TextStyle({fill: 0x00ff00, fontVariant: 'small-caps'});
        this.buttonText.anchor.set(0.5, 0.5);
        this.buttonText.x = 0;
        this.buttonText.y = 22;

        this.menu.addChild(this.menuTitle, this.restartButton);
        GameScreen.instance.screenObjects.addChild(this.menu);
    }

    public showStartMenu(): void {
        this.menu.visible = true;
    }
    
    public showGameOver(): void {
        this.menuTitle.text = "Game Over";
        this.buttonText.text = "restart";
        this.menu.visible = true;
    }

    public hideMenu(): void {
        this.menu.visible = false;
    }
}