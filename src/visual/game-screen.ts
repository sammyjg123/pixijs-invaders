import { Application, Container, Graphics } from "pixi.js";

export class GameScreen {

    static instance: GameScreen;

    public screenObjects: Container;
    
    private canvas: HTMLCanvasElement;
    private app: Application;
    public readonly canvasWidth: number;
    public readonly canvasHeight: number;

    constructor () {
        GameScreen.instance = this;

        this.canvasWidth = 1024;
        this.canvasHeight = 720;

        this.screenObjects = new Container();
        this.canvas = document.createElement("canvas");
        document.body.appendChild(this.canvas);
    
        this.app = new Application({ view: this.canvas, autoStart: false });
        this.app.renderer.backgroundColor = 0x333333;
        this.app.renderer.resize(this.canvasWidth, this.canvasHeight);
        
        this.app.stage.addChild(this.screenObjects);
    }

    public render(): void {

        this.app.render();
    }

}