import { Container, DisplayObject, Graphics, Polygon } from "pixi.js";
import { Collider } from "src/logic/collider";
import { EnemyShip } from "src/logic/enemy-ship";
import { PlayerShip } from "src/logic/player-ship";
import { Projectile } from "src/logic/projectile";
import { clamp, lerp, randomRange } from "src/utils/math-utils";
import { GameScreen } from "./game-screen";

export class BoardVisuals {

    readonly boardObjects: Container;

    private enemyShips: Container;
    private playerShip: Container;
    private projectiles: Container;
    private lastDrawnProjectiles: Projectile[];
    private lastDrawnEnemies: EnemyShip[];
    private firstRun: boolean;
    
    private boardFrame: Container;
    private boardMask: Container;
    private boardWidth: number;
    private boardHeight: number;

    // for debug purposes only
    private colliderGraphics: Container;

    constructor() {
        this.boardObjects = new Container();
        this.enemyShips = new Container();
        this.playerShip = new Container();
        this.boardFrame = new Container();
        this.boardMask = new Container();
        this.projectiles = new Container();
        this.lastDrawnProjectiles = [];
        this.lastDrawnEnemies = [];
        this.firstRun = true;

        this.colliderGraphics = new Container();

        this.boardWidth = 500;
        this.boardHeight = 700;
        this.drawFrame();
        this.projectiles.mask = this.boardMask;
        this.enemyShips.mask = this.boardMask;

        this.boardObjects.x = (GameScreen.instance.canvasWidth / 2) - (this.boardWidth / 2);
        this.boardObjects.y = (GameScreen.instance.canvasHeight / 2) - (this.boardHeight / 2);
        this.boardObjects.addChild(this.boardMask, this.boardFrame, this.colliderGraphics, this.projectiles, this.enemyShips, this.playerShip);
        GameScreen.instance.screenObjects.addChild(this.boardObjects);
    }

    public setPlayer(player: PlayerShip) {
        if (this.firstRun) {
            this.drawPlayer();
        }
        this.playerShip.x = player.positionX;
        this.playerShip.y = player.positionY;
        let shipSize: number = clamp(lerp(0.8, 1, (Date.now() - player.lastFireTime) / player.fireRate), 0.8, 1);
        this.playerShip.scale.x = shipSize;
        this.playerShip.scale.y = shipSize;
    }

    public drawProjectiles(projectileList: Projectile[]): void {
        if (projectileList != this.lastDrawnProjectiles) {
            this.projectiles.removeChildren();
            let projectileGraphic: Container;
            projectileList.forEach(projectile => {
                projectileGraphic = new Container();
                projectileGraphic.addChild(this.getDrawnProjectile(projectile));
                projectileGraphic.pivot.set(0.5, 0.5);
                this.projectiles.addChild(projectileGraphic);
            });
            this.lastDrawnProjectiles = projectileList;
        }
        
        for (let i = 0; i < this.lastDrawnProjectiles.length; i++) {
            let item: Projectile = this.lastDrawnProjectiles[i];
            this.projectiles.getChildAt(i).position.set(item.positionX, item.positionY);
        }
    }

    public drawEnemies(enemies: EnemyShip[], forceUpdate: boolean) {
        if (enemies != this.lastDrawnEnemies || forceUpdate) {
            this.enemyShips.removeChildren();
            let enemyGraphic: Container;
            enemies.forEach(enemy => {
                enemyGraphic = new Container();
                enemyGraphic.addChild(this.getDrawnEnemy(enemy));
                enemyGraphic.x = enemy.positionX;
                enemyGraphic.y = enemy.positionY;
                this.enemyShips.addChild(enemyGraphic);
            });
            this.lastDrawnEnemies = enemies;
        }

        for (let i = 0; i < this.lastDrawnEnemies.length; i++) {
            let item: EnemyShip = this.lastDrawnEnemies[i];
            let drawnItem: DisplayObject = this.enemyShips.getChildAt(i);
            drawnItem.position.set(item.positionX, item.positionY);
            drawnItem.rotation = this.lastDrawnEnemies[i].rotation;
        }
    }

    public drawColliders(colliders: Collider[]): void {
        this.colliderGraphics.removeChildren();
        let colliderGraphic: Graphics;
        colliders.forEach(collider => {
            colliderGraphic = new Graphics();
            colliderGraphic.beginFill(0xffffff);
            colliderGraphic.drawRect(collider.positionX, collider.positionY, collider.width, collider.height);
            colliderGraphic.endFill();
            this.colliderGraphics.addChild(colliderGraphic);
        });
    }

    private getDrawnProjectile(newProjectile: Projectile): Graphics {
        let projectile = new Graphics();
        if (newProjectile.isPlayerOwned) {
            projectile.moveTo((-newProjectile.width / 2) + 0.5, (newProjectile.height / 2) + 0.5);
            projectile.beginFill(0x114411);
            projectile.lineStyle(2, 0x00dd00, 1);
            projectile.lineTo((newProjectile.width / 2) + 0.5, (newProjectile.height / 2) + 0.5);
            projectile.lineTo(1, (-newProjectile.height / 2) + 0.5);
            projectile.lineTo((-newProjectile.width / 2) + 0.5, (newProjectile.height / 2) + 0.5);
            projectile.endFill();
        } else {
            projectile.moveTo((-newProjectile.width / 2) + 0.5, (-newProjectile.height / 2) + 0.5);
            projectile.beginFill(0x441111);
            projectile.lineStyle(2, 0xdd0000, 1);
            projectile.lineTo((newProjectile.width / 2) + 0.5, (-newProjectile.height / 2) + 0.5);
            projectile.lineTo(1, (newProjectile.height / 2) + 0.5);
            projectile.lineTo((-newProjectile.width / 2) + 0.5, (-newProjectile.height / 2) + 0.5);
            projectile.endFill();
        }
        
        return projectile;
    }

    private getDrawnEnemy(newEnemy: EnemyShip): Graphics {
        let enemy = new Graphics();
        let radius = newEnemy.width / 2;
        enemy.beginFill(0x552222);
        enemy.lineStyle(2, 0xff0000, 1);
        enemy.drawRegularPolygon(0, 0, radius, newEnemy.currentSides, Math.PI);
        enemy.endFill();
        return enemy;
    }

    private drawPlayer(): void {
        let ship: Graphics = new Graphics();
        ship.moveTo(10, 0);
        ship.beginFill(0x225522);
        ship.lineStyle(2, 0x00ff00, 1);
        ship.lineTo(20, 20);
        ship.lineTo(10, 15);
        ship.lineTo(0, 20);
        ship.lineTo(10, 0);
        ship.endFill();
        this.playerShip.pivot.set(10, 10);
        this.playerShip.addChild(ship);
        this.firstRun = false;
    }

    private drawFrame(): void {
        let frameMask = new Graphics();
        frameMask.beginFill(0x000000);
        frameMask.drawRoundedRect(0, 0, this.boardWidth, this.boardHeight, 8);
        frameMask.endFill();
        this.boardMask.addChild(frameMask);
        let frame = new Graphics();
        frame.beginFill(0x000000);
        frame.lineStyle(2, 0x00cc00, 1);
        frame.drawRoundedRect(0, 0, this.boardWidth, this.boardHeight, 8);
        frame.alpha = 0.5;
        frame.endFill();
        this.boardFrame.addChild(frame);
    }

}