import { over } from "lodash";
import { Container, Graphics, Text, TextStyle } from "pixi.js";
import { GameScreen } from "./game-screen";

export class BoardOverlay {

    private overlay: Container;
    private lifeDisplay: Container;
    private scoreDisplay: Text;

    private currentLives: number;
    private currentScore: number;

    constructor() {
        this.overlay = new Container();
        this.lifeDisplay = new Container();
        this.lifeDisplay.pivot.set(1, 0);
        this.lifeDisplay.x = (GameScreen.instance.canvasWidth / 2) + 200;
        this.lifeDisplay.y = (GameScreen.instance.canvasHeight / 2) - 345;
        this.scoreDisplay = new Text("");
        this.scoreDisplay.x = (GameScreen.instance.canvasWidth / 2) - 240;
        this.scoreDisplay.y = (GameScreen.instance.canvasHeight / 2) - 345;
        this.scoreDisplay.style = new TextStyle({fill: 0x00ff00, fontWeight: '600'});

        this.currentLives = 0;
        this.currentScore = -1;

        this.overlay.addChild(this.lifeDisplay, this.scoreDisplay);
        GameScreen.instance.screenObjects.addChild(this.overlay);
    }

    public updateScore(score: number) {
        if (score != this.currentScore) {
            this.scoreDisplay.text = score.toString();
            this.currentScore = score;
        }
    }

    public updateLives(lives: number) {
        if (lives != this.currentLives) {
            this.lifeDisplay.removeChildren();
            let ship: Container;
            for (let i = 0; i < lives; i++) {
                ship = this.getDrawnShip();
                ship.position.x = (-i + 1) * 25;
                this.lifeDisplay.addChild(ship);
            }
            this.currentLives = lives;
        }
    }

    private getDrawnShip(): Container {
        let ship: Graphics = new Graphics();
        ship.moveTo(10, 0);
        ship.beginFill(0x114411);
        ship.lineStyle(2, 0x00dd00, 1);
        ship.lineTo(20, 20);
        ship.lineTo(10, 15);
        ship.lineTo(0, 20);
        ship.lineTo(10, 0);
        ship.endFill();
        return ship;
    }
}