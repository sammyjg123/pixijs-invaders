import { randomInt } from "src/utils/math-utils";
import { CollisionType } from "./collision-manager";

export class Collider {
    public readonly colliderID: number;
    public collisionTypes: CollisionType[];

    public positionX: number;
    public positionY: number;
    public width: number;
    public height: number;

    public isTangible: boolean;
    public isPassive: boolean;
    public didCollide: boolean;

    constructor(originX: number, originY: number, width: number, height: number, passive: boolean = true, tangible: boolean = true) {
        this.colliderID = randomInt(1000000000);
        this.positionX = originX - (width / 2);
        this.positionY = originY - (height / 2);
        this.width = width;
        this.height = height;
        this.isPassive = passive;
        this.isTangible = tangible;
        this.didCollide = false;
        this.collisionTypes = [];
    }

    public updatePosition(x: number, y: number) {
        this.positionX = x - (this.width / 2);
        this.positionY = y - (this.height / 2);
    }

    public isCollision(types: CollisionType[]): boolean {
        for (let i = 1; i < types.length; i++) {
            if (this.collisionTypes[0] === types[i]) {
                return true;
            }
        }
        return false;
    }

    public onCollision(): void {
        this.didCollide = true;
    }
}