export class PlayerBounds {
    
    public positionX: number;
    public positionY: number;
    public width: number;
    public height: number;

    constructor(x: number, y: number, width: number, height: number) {
        this.positionX = x;
        this.positionY = y;
        this.width = width;
        this.height = height;
    }

}