import { Collider } from "./collider";
import { CollisionManager, CollisionType } from "./collision-manager";

export class Projectile {

    public isPlayerOwned: boolean;
    public verticalVelocity: number;
    public width: number;
    public height: number;
    public positionX: number;
    public positionY: number;
    public isExpired: boolean;
    
    private originX: number;
    private originY: number;
    private startTime: number;
    private extent: number;

    private collider: Collider;

    constructor(fromPlayer: boolean, velocity: number, originX: number, originY: number, width: number, height: number, extent: number) {

        this.isPlayerOwned = fromPlayer;
        this.verticalVelocity = velocity;
        this.width = width;
        this.height = height;
        this.positionX = originX;
        this.positionY = originY;
        this.extent = extent;

        this.isExpired = false;
        this.originX = originX;
        this.originY = originY;
        this.startTime = Date.now();

        this.collider = new Collider(this.originX, this.originY, this.width, this.height, false);
        if (fromPlayer) {
            this.collider.collisionTypes = [CollisionType.PlayerProjectile, CollisionType.EnemyShip];
        } else {
            this.collider.collisionTypes = [CollisionType.EnemyProjectile, CollisionType.PlayerShip];
        }

        CollisionManager.instance.addCollider(this.collider);
    }

    public moveProjectile(): void {
        let positionYDelta = ((Date.now() - this.startTime) * this.verticalVelocity) / 60;
        this.positionY = this.originY + positionYDelta;
        this.collider.updatePosition(this.positionX, this.positionY);
        if (Math.abs(positionYDelta) > this.extent || this.collider.didCollide) {
            this.isExpired = true;
        }
    }

    public onDestroy(): void {
        CollisionManager.instance.removeCollider(this.collider);
    }

}