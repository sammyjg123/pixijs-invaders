import { randomInt, randomRange } from "src/utils/math-utils";
import { EnemyShip } from "./enemy-ship";
import { EnemyType } from "./enemy-type";
import { Projectile } from "./projectile";

export class EnemyShipManager {
    
    public currentProjectiles: Projectile[];
    public enemyShips: EnemyShip[];
    public spawnWidth: number;
    public spawnPadding: number;
    public padding: number;
    public shipWidth: number;
    public shipHeight: number;
    public isDirty: boolean;
    public defaultMoveSpeed: number;

    public isMovingX: boolean;
    public moveSpeed: number;
    public moveDeltaX: number;
    public moveDeltaY: number;
    public moveExtentX: number;
    public moveExtentY: number;
    public enemiesWin: boolean;
    public deltaScore: number;
    
    private endY: number;
    private basicEnemyTypes: EnemyType[];
    private lastProjectileFired: number;

    constructor() {
        this.currentProjectiles = [];
        this.enemyShips = [];
        this.spawnWidth = 460;
        this.spawnPadding = 20;
        this.padding = 10;
        this.shipWidth = 20;
        this.shipHeight = 20;
        this.isDirty = false;
        this.lastProjectileFired = Date.now();
        this.defaultMoveSpeed = 1;

        this.isMovingX = true;
        this.moveSpeed = this.defaultMoveSpeed;
        this.moveDeltaX = 1;
        this.moveDeltaY = 0;
        this.moveExtentX = this.spawnWidth / 2;
        this.moveExtentY = this.shipWidth + this.padding;
        this.enemiesWin = false;
        this.deltaScore = 0;

        this.endY = 700 + this.shipHeight;
        this.basicEnemyTypes = [
            new EnemyType(3, 50, 3, 8, 40, 700),
            new EnemyType(4, 75, 6, 12, 25, 700),
            new EnemyType(5, 100, 8, 16, 10, 700),
            new EnemyType(6, 150, 3, 8, 70, 700),
            new EnemyType(7, 200, 6, 12, 40, 700),
            new EnemyType(8, 300, 8, 16, 50, 700)
        ];
    }

    public spawnEnemyWave(shipArray: number[][]): void {
        let enemy: EnemyShip;
        let positionX: number;
        let positionY: number;
        for (let row = 0; row < shipArray.length; row++) {
            for (let column = 0; column < shipArray[row].length; column++) {
                if (shipArray[row][column] > -1) {
                    positionX = (((this.shipWidth + this.padding) * column) + (this.spawnWidth / 2)) - (((this.shipWidth + this.padding) * shipArray[row].length) / 2) + (this.shipWidth - (this.padding / 2)) + this.spawnPadding;
                    positionY = ((this.shipHeight + this.padding) * row) - ((this.shipHeight + this.padding) * shipArray.length);
                    enemy = new EnemyShip(this.basicEnemyTypes[shipArray[row][column]], positionX, positionY, this.shipWidth, this.shipHeight);
                    this.enemyShips.push(enemy);
                }
            }
        }
    }

    public updateShips(difficulty: number): void {
        this.updateProjectiles();
        let shootFromIndex: number = -1;
        if (Date.now() - this.lastProjectileFired > randomRange(100, 8000)) {
            shootFromIndex = randomInt(this.enemyShips.length);
        }
        let removeIndexes: number[] = [];
        let canRemove: boolean = false;
        
        let lowestShip: number = 0;

        let furthestExtent: number = 0;
        let currentMoveAmountX = 0;
        let currentMoveAmountY = 0;
        if (this.isMovingX) {
            currentMoveAmountX = this.moveSpeed * this.moveDeltaX;
        } else {
            currentMoveAmountY = this.moveSpeed;
            furthestExtent = 999;
        }
        for (let i = 0; i < this.enemyShips.length; i++) {
            this.enemyShips[i].updatePosition(this.enemyShips[i].positionX + currentMoveAmountX, this.enemyShips[i].positionY + currentMoveAmountY);
            if (this.enemyShips[i].positionY > lowestShip) {
                lowestShip = this.enemyShips[i].positionY;
            }
            if (furthestExtent > this.enemyShips[i].positionY && !this.isMovingX) {
                furthestExtent = this.enemyShips[i].positionY;
            } else if (furthestExtent < Math.abs(this.enemyShips[i].positionX - this.moveExtentX) && this.isMovingX) {
                furthestExtent = Math.abs((this.enemyShips[i].positionX - this.spawnPadding) - this.moveExtentX);
            }
            if (this.enemyShips[i].collider.didCollide) {
                this.enemyShips[i].currentSides -= 1;
                this.enemyShips[i].collider.didCollide = false;
                this.isDirty = true;
                if (this.enemyShips[i].currentSides < 3) {
                    this.enemyShips[i].onDeath();
                    this.deltaScore += this.enemyShips[i].enemyType.scoreReward;
                    removeIndexes.push(i);
                    canRemove = true;
                }
            } else if (i === shootFromIndex) {
                if (this.enemyShips[i].positionY > 0) {
                    this.currentProjectiles.push(this.enemyShips[i].fire());
                    this.lastProjectileFired = Date.now();
                }
            }
        }

        if (this.isMovingX) {
            if (furthestExtent + Math.abs(this.moveDeltaX * this.defaultMoveSpeed) >= this.moveExtentX) {
                this.moveDeltaX *= -1;
                this.isMovingX = false;
            }
        } else {
            if (furthestExtent + this.defaultMoveSpeed >= this.moveExtentY) {
                this.moveExtentY += this.shipHeight + this.padding;
                this.isMovingX = true;
                this.moveSpeed *= 1 + (0.05 * difficulty);
            }
        }

        if (canRemove) {
            removeIndexes.forEach(index => {
                this.enemyShips.splice(index, 1);
            });
            this.isDirty = true;
        }
        if (lowestShip >= this.endY) {

        }
    }

    public reset(): void {
        this.moveSpeed = this.defaultMoveSpeed;
        this.moveDeltaY = 0;
        this.enemiesWin = false;
        this.moveExtentY = this.shipHeight + this.padding;
        this.currentProjectiles.forEach(projectile => {
            projectile.onDestroy();
        });
        this.enemyShips.forEach(ship => {
            ship.onDeath();
        });
        this.currentProjectiles = [];
        this.enemyShips = [];
    }

    private updateProjectiles(): void {
        for (let i = 0; i < this.currentProjectiles.length; i++) {
            this.currentProjectiles[0].moveProjectile();
            if (this.currentProjectiles[0].isExpired) {
                this.currentProjectiles[0].onDestroy();
                this.currentProjectiles.shift();
            } else {
                this.currentProjectiles.push(this.currentProjectiles.shift() as Projectile);
            }
        }
    }
}