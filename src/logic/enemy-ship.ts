import { randomRange } from "src/utils/math-utils";
import { Collider } from "./collider";
import { CollisionManager, CollisionType } from "./collision-manager";
import { EnemyType } from "./enemy-type";
import { Projectile } from "./projectile";

export class EnemyShip {

    public enemyType: EnemyType;
    public positionX: number;
    public positionY: number;
    public rotation: number;
    public width: number;
    public height: number;
    public currentSides: number;
    
    public collider: Collider;
    
    constructor(enemyType: EnemyType, x: number, y: number, width: number, height: number) {
        this.enemyType = enemyType;
        this.positionX = x;
        this.positionY = y;
        this.rotation = 0;
        this.width = width;
        this.height = height;
        this.currentSides = enemyType.edgeAmount;
        this.collider = new Collider(this.positionX, this.positionY, this.width, this.height);
        this.collider.collisionTypes = [CollisionType.EnemyShip, CollisionType.PlayerProjectile, CollisionType.PlayerShip];
        CollisionManager.instance.addCollider(this.collider);
    }

    public onDeath() {
        CollisionManager.instance.removeCollider(this.collider);
    }

    public fire(): Projectile {
        return new Projectile(
            false,
            this.enemyType.projectileVelocity,
            this.positionX,
            this.positionY + this.height,
            this.enemyType.projectileWidth,
            this.enemyType.projectileHeight,
            this.enemyType.projectileExtent
        );
    }

    public updatePosition(x: number, y: number): void {
        this.positionX = x;
        this.positionY = y;
        this.rotation = (this.rotation + randomRange(0.04, 0.05)) % (2* Math.PI);
        this.collider.updatePosition(this.positionX, this.positionY);
    }

}