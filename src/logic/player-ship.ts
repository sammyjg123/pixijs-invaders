import { clamp } from "src/utils/math-utils";
import { Collider } from "./collider";
import { CollisionManager, CollisionType } from "./collision-manager";
import { PlayerBounds } from "./player-bounds";
import { Projectile } from "./projectile";

export class PlayerShip {

    public bounds: PlayerBounds;
    public positionX: number;
    public positionY: number;
    public playerwidth: number;
    public playerHeight: number;
    public playerLives: number;
    public currentPlayerLives: number;
    public playerDied: boolean;
    public currentProjectiles: Projectile[];
    public lastFireTime: number;
    public fireRate: number;

    private fireVelocity: number;
    private projectileHeight: number;
    private projectileWidth: number;

    private projectileExtent: number;
    private moveVelocity: number;
    private collider: Collider;

    constructor() {
        this.bounds = new PlayerBounds(20, 600, 460, 80);
        this.positionX = this.bounds.positionX + (this.bounds.width / 2);
        this.positionY = this.bounds.positionY + (this.bounds.height / 2);
        this.playerwidth = 20;
        this.playerHeight = 20;
        this.playerLives = 5;
        this.currentPlayerLives = this.playerLives;
        this.playerDied = false;

        this.currentProjectiles = [];
        this.fireVelocity = -50;
        this.projectileHeight = 8;
        this.projectileWidth = 3;

        this.projectileExtent = 700;
        this.fireRate = 300; // measured in shots per millisecond
        this.moveVelocity = 5;
        this.lastFireTime = 0;
        this.collider = new Collider(this.positionX, this.positionY, this.playerwidth, this.playerHeight)
        this.collider.collisionTypes = [CollisionType.PlayerShip, CollisionType.EnemyProjectile, CollisionType.Pickup, CollisionType.EnemyShip];

        CollisionManager.instance.addCollider(this.collider);
    }

    public moveHorizontal(amount: number): void {
        amount *= this.moveVelocity;
        this.positionX = clamp(this.positionX + amount, this.bounds.positionX, this.bounds.positionX + this.bounds.width);
    }

    public moveVertical(amount: number): void {
        amount *= this.moveVelocity;
        this.positionY = clamp(this.positionY + amount, this.bounds.positionY, this.bounds.positionY + this.bounds.height);
    }

    public wasHit(): void {
        this.collider.didCollide = false;
        this.currentPlayerLives -= 1;
        if (this.currentPlayerLives === 0) {
            this.playerDied = true;
        }
    }

    public updatePlayer(): void {
        for (let i = 0; i < this.currentProjectiles.length; i++) {
            this.currentProjectiles[0].moveProjectile();
            if (this.currentProjectiles[0].isExpired) {
                this.currentProjectiles[0].onDestroy();
                this.currentProjectiles.shift();
            } else {
                this.currentProjectiles.push(this.currentProjectiles.shift() as Projectile);
            }
        }
        this.collider.updatePosition(this.positionX, this.positionY);
        if (this.collider.didCollide) {
            this.wasHit();
        }
    }

    public fire(): void {
        if (Date.now() > this.lastFireTime + this.fireRate) {
            let projectile = new Projectile(true, this.fireVelocity, this.positionX, this.positionY - this.playerHeight, this.projectileWidth, this.projectileHeight, this.projectileExtent);
            this.currentProjectiles.push(projectile);
            this.lastFireTime = Date.now();
        }
    }

    public reset(): void {
        this.currentPlayerLives = this.playerLives;
        this.currentProjectiles.forEach(projectile => {
            projectile.onDestroy();
        });
        this.playerDied = false;
    }

}