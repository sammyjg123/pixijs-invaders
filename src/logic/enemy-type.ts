export class EnemyType {
    public edgeAmount: number;
    public scoreReward: number;

    public projectileWidth: number;
    public projectileHeight: number;
    public projectileVelocity: number;
    public projectileExtent: number;

    constructor(edges: number, points: number, projectileWidth: number, projectileHeight: number, projectileVelocity: number, projectileExtent: number) {
        this.edgeAmount = edges;
        this.scoreReward = points;

        this.projectileWidth = projectileWidth;
        this.projectileHeight = projectileHeight;
        this.projectileVelocity = projectileVelocity;
        this.projectileExtent = projectileExtent;
    }
}