import { Container } from "pixi.js";
import { Collider } from "./collider";

export enum CollisionType {
    PlayerShip = 0,
    PlayerProjectile = 1,
    EnemyShip = 2,
    EnemyProjectile = 3,
    Pickup = 4
}

export class CollisionManager {

    public static instance: CollisionManager;

    public activeColliders: Collider[];

    constructor() {
        CollisionManager.instance = this;
        this.activeColliders = [];
    }

    public addCollider(collider: Collider): void {
        this.activeColliders.push(collider);
    }

    public removeCollider(collider: Collider): void {
        for (let i = 0; i < this.activeColliders.length; i++) {
            if (this.activeColliders[i].colliderID === collider.colliderID) {
                this.activeColliders.splice(i, 1);
                break;
            }
        };
    }

    public checkCollisions(): void {
        let activeRectEndX: number;
        let activeRectEndY: number;
        let passiveRectEndX: number;
        let passiveRectEndY: number;
        let xOverlap: boolean = false;
        let yOverlap: boolean = false;
        this.activeColliders.forEach(activeCollider => {
            activeRectEndX = activeCollider.positionX + activeCollider.width;
            activeRectEndY = activeCollider.positionY + activeCollider.height;
            this.activeColliders.forEach(passiveCollider => {
                if (passiveCollider != activeCollider) {
                    passiveRectEndX = passiveCollider.positionX + passiveCollider.width;
                    passiveRectEndY = passiveCollider.positionY + passiveCollider.height;
                    if ((passiveCollider.positionX > activeCollider.positionX && passiveCollider.positionX < activeRectEndX) ||
                        (passiveRectEndX > activeCollider.positionX && passiveRectEndX < activeRectEndX)) {
                            xOverlap = true;
                    }
                    if ((passiveCollider.positionY > activeCollider.positionY && passiveCollider.positionY < activeRectEndY) ||
                        (passiveRectEndY > activeCollider.positionY && passiveRectEndY < activeRectEndY)) {
                            yOverlap = true;
                    }

                    if (xOverlap && yOverlap) {
                        if (passiveCollider.isCollision(activeCollider.collisionTypes)) {
                            passiveCollider.onCollision();
                        }
                        if (activeCollider.isCollision(passiveCollider.collisionTypes)) {
                            activeCollider.onCollision();
                        }
                    }

                    xOverlap = false;
                    yOverlap = false;
                }
            });
        });
    }

}