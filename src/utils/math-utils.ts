
export const randomRange = (min: number, max: number): number => {
    return (Math.random() * (max - min)) + min;
};

export const randomInt = (max: number): number => {
    return Math.floor(Math.random() * max);
};

export const lerp = (start: number, end: number, time: number): number => {
    return start + (time * (end - start));
};

export const clamp = (value: number, min: number, max: number): number => {
    return Math.min(Math.max(value, min), max);
};